//
//  SecondViewController.swift
//  MultiscreenPOC
//
//  Created by Stuart Grimshaw on 11/04/16.
//  Copyright © 2016 Stuart Grimshaw. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController
{
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var leftTableView: UITableView!
    @IBOutlet weak var rightTableView: UITableView!

    @IBAction func focusGuideOnOffChanged(sender: UISegmentedControl) {print(sender.selectedSegmentIndex)}
    
    @IBAction func buttonPrimaryAction(sender: AnyObject) {print(self, #function)}
    @IBAction func buttonTouchDown(sender: AnyObject){print(self, #function)}
    @IBAction func buttonTouchUpInside(sender: AnyObject) {print(self, #function)}

    override func viewDidLoad()
    {
        super.viewDidLoad()
        leftTableView.dataSource = self
        leftTableView.delegate = self
        rightTableView.dataSource = self
        rightTableView.delegate = self
        
        leftTableView.remembersLastFocusedIndexPath = true
        rightTableView.remembersLastFocusedIndexPath = true
    }
    
    override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        print(#function)
        print("PREVIOUS VIEW", context.previouslyFocusedView)
        print("NEXT VIEW", context.nextFocusedView)
        print("")
    }

}

extension SecondViewController: UITableViewDataSource
{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = "Item \(indexPath.row)"
        cell.backgroundColor = UIColor.brownColor()
        return cell
    }
}

extension SecondViewController: UITableViewDelegate
{
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(#function, indexPath)
    }
    
    func tableView(tableView: UITableView, didUpdateFocusInContext context: UITableViewFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        print(#function)
        print("PREVIOUS VIEW", context.previouslyFocusedView)
        print("PREVIOUS PATH", context.previouslyFocusedIndexPath)
        print("NEXT VIEW", context.nextFocusedView)
        print("NEXT VIEW", context.nextFocusedIndexPath)
        print("")
    }
}